import logging
import traceback as tb
import suds.metrics as smetrics
from suds import WebFault
from suds.client import Client

url = "http://115.112.85.58/scfuadmin/ISCFMEndpoints?wsdl"
user = "BSIMMaker"
pwd = "a1c22784c4618b345680d4f42459cf725faadd43c395208f40e112421fa319d83c0295285b11a26bbd365de78af6e166e7603638bfea29f03bf56857552a1504"


try:
    client = Client(url)
    print(client)
    token = client.service.login(user, pwd)
    print("Token is {0}".format(token))
except WebFault as wf:
    print(wf)
    print(wf.fault)
except Exception as ex:
    print(ex)
    tb.print_exc()

print("Done")
