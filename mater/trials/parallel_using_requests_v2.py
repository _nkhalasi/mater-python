import asyncio
import requests
from datetime import datetime as dt
import pprint


pp = pprint.PrettyPrinter(indent=4)


def log(msg):
    print("{0} :: {1}".format(dt.now(), msg))


async def fetch_pages(urls, loop):
    futures = [(url, fetch_page(url, loop)) for url in urls]
    responses = map(lambda url, fut: (url, process_response(fut)), futures)
    print("Waiting to process responses")
    map(lambda url, res: log("{0} -> Fetch done".format(url)), responses)


def fetch_page(url, loop):
    log("{0} -> Start fetching".format(url))
    return loop.run_in_executor(None, requests.get, url)


async def process_response(fut):
    return await fut


def main():
    start = dt.now()
    from test_urls import urls
    loop = asyncio.new_event_loop()
    loop.run_until_complete(fetch_pages(urls, loop))
    log("Total time {0}".format(str(dt.now() - start)))

if __name__ == '__main__':
    main()
