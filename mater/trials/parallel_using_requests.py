import asyncio
import requests
from datetime import datetime as dt
import pprint


pp = pprint.PrettyPrinter(indent=4)


def log(msg):
    print("{0} :: {1}".format(dt.now(), msg))


def fetch_pages(urls, loop):
    fetch_tasks = [asyncio.ensure_future(timed_request(url)) for url in urls]
    done_tasks, pending_tasks = loop.run_until_complete(asyncio.wait(fetch_tasks))
    timings = {}
    for task in done_tasks:
        url, _, timing = task.result()
        timings[url] = str(timing)
    return timings


async def timed_request(url):
    log("{0} -> Start timing fetch".format(url))
    start = dt.now()
    res_details = await asyncio.ensure_future(fetch_page(url))
    # res_details = await fetch_page(url)
    log("{0} -> Timing done".format(url))
    return url, res_details, dt.now() - start


async def fetch_page(url):
    try:
        log("{0} -> Start fetching".format(url))
        response = requests.get(url)
        log("{0} -> Fetching done".format(url))
        assert response.status_code == 200
        return response.text
    except requests.exceptions.ConnectionError as ce:
        return None


def main():
    start = dt.now()
    from test_urls import urls
    loop = asyncio.new_event_loop()
    try:
        asyncio.set_event_loop(loop)
        x = fetch_pages(urls, loop)
        pp.pprint(x)
    finally:
        loop.close()
    log("Total time {0}".format(str(dt.now() - start)))

if __name__ == '__main__':
    main()

    # @asyncio.coroutine
    # def factorial(name, number):
    #     f = 1
    #     for i in range(2, number+1):
    #         print("Task %s: Compute factorial(%s)..." % (name, i))
    #         yield from asyncio.sleep(1)
    #         f *= i
    #     print("Task %s: factorial(%s) = %s" % (name, number, f))

    # loop = asyncio.get_event_loop()
    # tasks = [
    #     asyncio.ensure_future(factorial("A", 2)),
    #     asyncio.ensure_future(factorial("B", 3)),
    #     asyncio.ensure_future(factorial("C", 4))]
    # loop.run_until_complete(asyncio.wait(tasks))
    # loop.close()
