import asyncio
import aiohttp
from datetime import datetime as dt
from toolz import curry


def log(msg):
    print("{0} :: {1}".format(dt.now(), msg))


# def fetch_pages(urls, loop):
#     fetch_tasks = [asyncio.ensure_future(fetch_page(aiohttp.ClientSession())(url)) for url in urls]
#     done_tasks, pending_tasks = loop.run_until_complete(asyncio.wait(fetch_tasks))


# @curry
# async def fetch_page(client, url):
#     async with client as c:
#         log("{0} -> Starting fetch".format(url))
#         async with c.get(url) as resp:
#             assert resp.status == 200
#             log("{0} -> Fetch done".format(url))
#             return await resp.text()


def fetch_pages(urls, loop):
    with aiohttp.ClientSession() as client:
        page_fetcher = fetch_page(client)
        fetch_tasks = [asyncio.ensure_future(page_fetcher(url)) for url in urls]
        done_tasks, pending_tasks = loop.run_until_complete(asyncio.wait(fetch_tasks))


@curry
async def fetch_page(client, url):
    log("{0} -> Starting fetch".format(url))
    async with client.get(url) as resp:
        assert resp.status == 200
        log("{0} -> Fetch done".format(url))
        return await resp.text()


def main():
    start = dt.now()
    from test_urls import urls
    loop = asyncio.new_event_loop()
    try:
        asyncio.set_event_loop(loop)
        fetch_pages(urls, loop)
    finally:
        loop.close()
    log("Total time {0}".format(str(dt.now() - start)))

if __name__ == '__main__':
    main()
