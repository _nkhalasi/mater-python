import logging
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import asyncio
import aiohttp
from datetime import datetime as dt
import os
from toolz import curry

LOG = logging.getLogger(__name__)

FR_OWNERS = ("DealerP", "DealerT", "DealerU", "DealerM", "DealerN")
# VAYANA_URL = "https://gamma-demo.vayananet.com"
VAYANA_URL = "https://plutoweb.nnote"
VAYANA_USER = "vayana/vadmin1"
VAYANA_PASS = "Bhel24pani#Puri"


def tick():
    LOG.debug("Tick! The time is: {0}".format(dt.now()))


def schedule_coroutine(target, *, loop=None):
    """Schedules target coroutine in the given event loop

    If not given, *loop* defaults to the current thread's event loop

    Returns the scheduled task.
    """
    if asyncio.iscoroutine(target):
        return asyncio.ensure_future(target, loop=loop)
    raise TypeError("target must be a coroutine, "
                    "not {!r}".format(type(target)))


async def download_frs_for(fr_owner):
    url = "{0}/integration/v1/FundingRequests".format(VAYANA_URL)
    LOG.debug("Downloading FRs for {0}".format(fr_owner))
    headers = {"Accept": "application/xml"}
    auth = aiohttp.BasicAuth(VAYANA_USER, password=VAYANA_PASS)
    connector = aiohttp.TCPConnector(verify_ssl=False)
    async with aiohttp.ClientSession(headers=headers, auth=auth, connector=connector) as client:
        try:
            async with client.get("{0}?entity={1}".format(url, fr_owner)) as resp:
                return await resp.text()
        except aiohttp.errors.ClientOSError as cex:
            return "Unable to connect"


async def trigger_download(fr_owner):
    LOG.debug("Triggering FR download for {0}".format(fr_owner))
    print(await download_frs_for(fr_owner))


def get_frs_for(loop, fr_owner):
    def _get_frs_for():
        schedule_coroutine(trigger_download(fr_owner), loop=loop)
    return _get_frs_for


def start_scheduler():
    scheduler = AsyncIOScheduler()
    loop = asyncio.get_event_loop()

    for x in FR_OWNERS:
        fr_getter = get_frs_for(loop, x)
        scheduler.add_job(fr_getter, "interval", seconds=30)
    scheduler.start()
    LOG.debug("Press Ctrl+{0} to exit".format("Break" if os.name == "nt" else "C"))

    try:
        asyncio.get_event_loop().run_forever()
    except (KeyboardInterrupt, SystemExit) as ex:
        pass
