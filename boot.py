def init_logging(logging_config_file):
    import logging.config
    logging.config.fileConfig(logging_config_file)


def start_scheduler():
    import mater.scheduler
    mater.scheduler.start_scheduler()

if __name__ == "__main__":
    init_logging("mater-logging.ini")
    start_scheduler()
